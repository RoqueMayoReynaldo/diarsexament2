﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using T2Application.DB;
using T2Application.Models;

namespace T2Application.Controllers
{
    public class AdminController : Controller
    {
        private IWebHostEnvironment hosting;
        private AppT2DiarsContext context;
        public AdminController(AppT2DiarsContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }



        [HttpGet]
        public ActionResult CrearPokemon()
        {
            ViewBag.Tipos = context.Tipos.ToList();
           
            return View(new Pokemon());

        }

        [HttpPost]
        public IActionResult CrearPokemon(Pokemon pokemon, IFormFile file)
        {
            var pok = context.Pokemons.FirstOrDefault(o => o.nombre == pokemon.nombre);
            if (pok != null)
            {
                ModelState.AddModelError("nombre", "el nombre ya esta en uso");
            }
            if (file ==null)
            {
                ModelState.AddModelError("imagen", "imagen pokemon requerida ");
            }
            if (ModelState.IsValid)
            {

                pokemon.imagen  = saveFile(file);
                context.Add(pokemon);
                context.SaveChanges();
             
          
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Tipos = context.Tipos.ToList();
            return View("CrearPokemon",pokemon);
        }


        private string saveFile(IFormFile file)
        {

           
            string relativePath = null;
            if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpeg"))
            {
                relativePath = @"\files\" + file.FileName;
                string ruta = Path.Combine(hosting.WebRootPath, "files", file.FileName);
                FileStream stream = new FileStream(ruta, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();

            }
            return relativePath.Replace("\\", "/");
        }



    }
}
