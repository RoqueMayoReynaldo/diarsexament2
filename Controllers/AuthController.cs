﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using T2Application.DB;
using T2Application.Models;
namespace T2Application.Controllers
{
    public class AuthController : Controller
    {

        private AppT2DiarsContext context;

        
        public AuthController(AppT2DiarsContext context)
        {
            this.context = context;

        }
      

        [HttpGet]
        public ActionResult Login()
        {

            return View("Login");
        }

        [HttpPost]
        public IActionResult Login(string pass, string user)
        {

            Usuario userN = context.Usuarios.FirstOrDefault(Usuario => Usuario.userName == user && Usuario.contraseña == pass);
            if (userN == null)

            {
                ModelState.AddModelError("Mensaje", "Porfavor verifique sus datos de ingreso");
            }

            if (ModelState.IsValid)
            {
                var claims = new List<Claim> {
                new Claim(ClaimTypes.Name,user) };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);


                return RedirectToAction("Index", "Home");
            }
            else
            {

                return Login();
            }




        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();



            return View("Login");
        }


    }
}
