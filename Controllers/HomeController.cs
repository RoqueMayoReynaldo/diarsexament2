﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using T2Application.Models;
using T2Application.DB;
namespace T2Application.Controllers
{
    public class HomeController : Controller
    {
       

        private AppT2DiarsContext context;
        public HomeController(AppT2DiarsContext context)
        {
            this.context = context;
        }




        [HttpGet]
        public IActionResult Index()
        {
            List<Pokemon> pokemones = context.Pokemons.ToList();
            return View("Index",pokemones);
        }
        [HttpPost]
        public IActionResult Index(string buscar)
        {
            if (string.IsNullOrEmpty(buscar))
            {
                return Index();
            }
            List<Pokemon> pokemones = context.Pokemons.Where(o=>o.nombre==buscar).ToList();
         
            return View("Index", pokemones);

        }

    }
}
