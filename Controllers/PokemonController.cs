﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.DB;
using T2Application.Models;
namespace T2Application.Controllers
{
    [Authorize]
    public class PokemonController : Controller
    {
        private AppT2DiarsContext context;
        public PokemonController(AppT2DiarsContext context)
        {
            this.context = context;

        }

        [HttpGet]
        public IActionResult Liberar(int id)
        {
            UsuarioPokemon registro = context.UsuarioPokemons.FirstOrDefault(UsuarioPokemon=> UsuarioPokemon.id==id);
            context.UsuarioPokemons.Remove(registro);
            context.SaveChanges();



            return Capturados();
        }

        [HttpGet]
        public IActionResult Capturados()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            Usuario usuario = context.Usuarios.Include("usuarioPokemones.pokemon").First(o => o.userName == username); ;



            return View("Capturados",usuario);
        }


        [HttpGet]
        public IActionResult Capturar(int id)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            Usuario usuario = context.Usuarios.First(o => o.userName == username);


            UsuarioPokemon up = new UsuarioPokemon
            {
                idUsuario = usuario.id,
                idPokemon = id,
                fecha = DateTime.Now
            };
            context.UsuarioPokemons.Add(up);
            context.SaveChanges();

            return RedirectToAction("Index","Home");
        }

      

    }
}
