﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.DB.Mapping;
using T2Application.Models;
namespace T2Application.DB
{
    public class AppT2DiarsContext : DbContext
    {
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioPokemon> UsuarioPokemons { get; set; }
        public AppT2DiarsContext(DbContextOptions<AppT2DiarsContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new TipoMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new UsuarioPokemonMap());
        }
    }
}
