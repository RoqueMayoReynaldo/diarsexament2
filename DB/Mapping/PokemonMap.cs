﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.Models;

namespace T2Application.DB.Mapping
{
   
    public class PokemonMap : IEntityTypeConfiguration<Pokemon>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Pokemon> builder)
        {
            builder.ToTable("Pokemon", "dbo");
            builder.HasKey(Pokemon => Pokemon.id);
        }
    }
}


