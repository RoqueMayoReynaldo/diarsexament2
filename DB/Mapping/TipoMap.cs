﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.Models;

namespace T2Application.DB.Mapping
{

    public class TipoMap : IEntityTypeConfiguration<Tipo>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Tipo> builder)
        {
            builder.ToTable("Tipos", "dbo");
            builder.HasKey(Tipo => Tipo.id);
        }
    }
}

