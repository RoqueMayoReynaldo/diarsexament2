﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.Models;

namespace T2Application.DB.Mapping
{

    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario", "dbo");
            builder.HasKey(Usuario => Usuario.id);
            builder.HasMany(Usuario => Usuario.usuarioPokemones).WithOne().HasForeignKey(UsuarioPokemon=>UsuarioPokemon.idUsuario);
        }

    }
}


