﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T2Application.Models;

namespace T2Application.DB.Mapping
{

    public class UsuarioPokemonMap : IEntityTypeConfiguration<UsuarioPokemon>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<UsuarioPokemon> builder)
        {
            builder.ToTable("UsuarioPokemon", "dbo");
            builder.HasKey(UsuarioPokemon => UsuarioPokemon.id);
            builder.HasOne(UsuarioPokemon=> UsuarioPokemon.pokemon).WithMany().HasForeignKey(UsuarioPokemon=> UsuarioPokemon.idPokemon);
        }
    }
}


