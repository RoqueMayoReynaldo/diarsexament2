﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace T2Application.Models
{
    public class Pokemon
    {

        public int id { get; set; }
        [Required(ErrorMessage = "Nombre requerido")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "Tipo requerido")]
        public string tipo { get; set; }
       
        public string imagen { get; set; }
    }
}
