﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2Application.Models
{
    public class Tipo
    {
        public int id { get; set; }
        public string nombre { get; set; }
    }
}
