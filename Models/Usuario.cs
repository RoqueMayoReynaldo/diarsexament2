﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2Application.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string contraseña { get; set; }
        public List<UsuarioPokemon> usuarioPokemones{get;set;}  

    }
}
