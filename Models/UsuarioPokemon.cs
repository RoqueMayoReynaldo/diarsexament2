﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T2Application.Models
{
    public class UsuarioPokemon
    {
        public int id { get; set; }
        public int idUsuario { get; set; }
        public int idPokemon { get; set; }
        public DateTime fecha { get; set; }
        public Pokemon pokemon { get; set; }
    }
}
